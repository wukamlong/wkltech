---
title: "Echo Dot 4"
date: 2021-09-18T16:59:34-03:00
draft: false
---

A melhor *alexa* para o seu bolso!

![Echo](https://images3.kabum.com.br/produtos/fotos/129093/smart-speaker-home-echo-dot-amazon-alexa-4-geracao-preto-b084dwczy6_1601639278_g.jpg)

## Porque comprar?
#### Possui um preço próximo com relação a sua versão anterior, 
#### apesar de não conter muitas features a mais, mas que justificam 
#### seu preço que é um som melhor e seu sensor ultrassônico!
> -----------------------------------------------------------------
## Prós:
#### Som frontal, em relação ao surround da Echo Dot 3
#### Design bem construído e mais bonito
#### Possui sensor ultrassônico bem divertido
> -----------------------------------------------------------------
## Contras:
#### Não conecta o som no cabo, apenas para saída de som externo
#### Um pouco mais surda
#### Cabo de carregamento curto


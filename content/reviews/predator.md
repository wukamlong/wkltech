---
title: "Predator Helios 300"
date: 2021-09-18T16:59:34-03:00
draft: false
---
Top de linha de notebooks da Acer no Brasil!

![Predator](https://i.pcmag.com/imagery/reviews/07MP0C7dIf4k4wcrFwdHAtm-3..v1569469929.jpg)
## Porque comprar?
#### Mesmo sendo caro, ele oferece poder de desempenho semelhante de 
#### concorrentes que cobram até 30% a mais. Depois do Nitro 5, ele
#### o rei do custo-benefício. A versão apresentada é a de 2019 e não
#### a atual que apresenta problemas de aquecimento.
> -----------------------------------------------------------------
## Especificações importantes:
#### Tela Full HD 15.6' 1920 x 1080 144Hz
#### Processador intel I7 11ª geração
#### 16gb de RAM single channel
#### Placa de vídeo dedicada RTX2070 max-q 8gb
> -----------------------------------------------------------------
## Prós:
#### Tampa e Chassi de alumínio
#### Boa resolução e taxa de atualização
#### Desempenho alto
#### Sistema de refrigeração eficiente
> -----------------------------------------------------------------
## Contras:
#### Construção peca na parte da tela
#### Tinta das teclas saem fácil com o uso
#### Ventoinhas no máximo fazem muito barulho
#### Modo turbo não faz muita diferença

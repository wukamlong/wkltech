---
title: "Monitor Acer Gamer Nitro RG241Y"
date: 2021-09-18T16:59:34-03:00
draft: false
---
Monitor com boa qualidade e "barato" da Acer!

![Monitor](https://images7.kabum.com.br/produtos/fotos/sync_mirakl/132387/Monitor-Gamer-Acer-23-8-FHD-FreeSync-HDR10-1ms-165Hz-IPS-Zero-frame-RG241Y_1657284382_g.jpg)
## Porque comprar?
#### O monitor possui uma faixa mediana de preço para aqueles que 
#### querem uma alternativa para os queridos monitores da marca AOC
#### tendo uma marca de qualidade e com menos problemas, sem contar
#### com as boas especificações do monitor.
> -----------------------------------------------------------------
## Especificações importantes:
#### Resolução Full HD 1920 x 1080
#### Tecnologia IPS ao invés de VA
#### Taxa de atualização de 165 Hz
#### Tamanho 23.8' 16:9
> -----------------------------------------------------------------
## Prós:
#### Boa resolução
#### Tecnologia IPS funciona bem
#### Pouco Ghosting
#### HDR funciona bem e fica com cores bem vibrantes
#### Framerate muito bom
> -----------------------------------------------------------------
## Contras:
#### Construção em geral é boa, mas peca em algumas partes
#### V-Sync não funciona muito bem




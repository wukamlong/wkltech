---
title: "Sobre mim"
date: 2021-09-18T16:59:34-03:00
draft: false
---
\
Wu Kam Long, nascido na cidade de Santos (cheio de haters de Corinthianos 😒).

![Santos](https://cdn.diariodolitoral.com.br/upload/dn_noticia/2020/01/orla-santos-tadeu-nascimento_1.jpg)

Sempre foi apaixonado por tecnologias novas e automação doméstica baseada em tecnologias de inteligência artificial e tecnologia Tuya, \
focado nesse tipo de comunicação entre aparelhos. Diversos gadgets foram avaliados e aqui vocês poderão ter esse prazer de ler algo \
sucinto e direto sobre produtos que devem ser de interesse de nossos leitores afim de melhorar a sua qualidade de vida!

![Casa](https://blog.leveros.com.br/wp-content/uploads/2020/03/folder_leveros_casa_conectada.png)



